/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.afipmaveninspt;

import java.sql.SQLException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Skynet
 */
public class PersonaRepositoryMySQLTest {
    
    public PersonaRepositoryMySQLTest() {
    }

    @org.junit.BeforeClass
    public static void setUpClass() throws Exception {
    }

    @org.junit.AfterClass
    public static void tearDownClass() throws Exception {
    }

    @org.junit.Before
    public void setUp() throws Exception {
    }

    @org.junit.After
    public void tearDown() throws Exception {
    }
   
    /**
     * Test of findPersonaByDNI method, of class PersonaRepositoryMySQL.
     */
    
    @Test(expected = SQLException.class)
    public void testFindPersonaByDNI() throws SQLException {
        System.out.println("findPersonaByDNI");
        String dni = "40258245";
        PersonaRepositoryMySQL instance = new PersonaRepositoryMySQL("afip", "127.0.0.1","","");
        String expResult = "Martin";
        String result = instance.findPersonaByDNI(dni);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of dividir method, of class PersonaRepositoryMySQL.
     */
    @Test(expected = ArithmeticException.class)
    public void testDividir() {
        System.out.println("dividir");
        int a = 0;
        int b = 0;
        PersonaRepositoryMySQL instance = new PersonaRepositoryMySQL("afip", "127.0.0.1","","");
        int expResult = 0;
        int result = instance.dividir(a, b);


    }
    
   
}
