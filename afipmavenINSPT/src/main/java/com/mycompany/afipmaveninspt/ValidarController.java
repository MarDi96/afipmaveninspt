/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.afipmaveninspt;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Skynet
 */
@Controller
public class ValidarController {
  @Autowired
  private PersonaRepository pR;
  
  @PostMapping("/validar")
  public String validar(Model modelo,@RequestParam(value="DNI") String DNI){
      String respuesta= pR.findPersonaByDNI(DNI);
      
      if (respuesta.isEmpty()){
          modelo.addAttribute("mensaje", "Persona no encontrada");
         
      }else{
          modelo.addAttribute("mensaje", "se encontró a " + respuesta);
      }
      
      return "respuesta";
  }
}
