/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.afipmaveninspt;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Skynet
 */
@Repository
public class PersonaRepositoryMySQL implements PersonaRepository {

    private final String dbFullURL;
    private final String dbUser;
    private final String dbPswd;

    @Autowired
    public PersonaRepositoryMySQL(@Qualifier("dbName") String dbName,
            @Qualifier("dbURL") String dbURL,
            @Qualifier("dbUser") String dbUser,
            @Qualifier("dbPswd") String dbPswd) {

        dbFullURL = "jdbc:mysql://" + dbURL + "/" + dbName;
        this.dbUser = dbUser;
        this.dbPswd = dbPswd;
    }

    @Override
    public String findPersonaByDNI(String dni){
        String resultado = "";
        try {
            Connection con = DriverManager.getConnection(dbFullURL, dbUser, dbPswd);
            Statement stmt = con.createStatement();
            stmt.execute("SELECT nombre FROM personas WHERE DNI='" + dni + "';");
            ResultSet rs = stmt.getResultSet();
            if (rs.next()) {
                resultado = rs.getString(1);
            }
            con.close();
        } catch (SQLException e) {
            resultado = e.getMessage();
            //System.err.println(e.getMessage());
        }
        return resultado;
    }
    
    public int dividir(int a, int b){
        return a/b;
    }

}
