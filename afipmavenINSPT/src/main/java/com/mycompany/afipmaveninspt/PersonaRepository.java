/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.afipmaveninspt;

import java.sql.SQLException;

/**
 *
 * @author Skynet
 */
public interface PersonaRepository {
    public String findPersonaByDNI(String dni);
}
